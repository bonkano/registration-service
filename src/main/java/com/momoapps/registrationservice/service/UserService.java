package com.momoapps.registrationservice.service;

import java.util.Collection;
import java.util.Optional;

import com.momoapps.registrationservice.exception.BusinessResourceException;
import com.momoapps.registrationservice.model.User;

public interface UserService {

	Collection<User> getAllUsers();

	Optional<User> findUserById(Long id) throws BusinessResourceException;

	Optional<User> findByLogin(String login) throws BusinessResourceException;

	User saveOrUpdateUser(User user) throws BusinessResourceException;

	void deleteUser(Long id) throws BusinessResourceException;

	Optional<User> findByLoginAndPassword(String login, String password) throws BusinessResourceException;

}