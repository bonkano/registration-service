package com.momoapps.registrationservice.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.momoapps.registrationservice.model.User;

@RestResource
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByLogin(String loginParam);
}