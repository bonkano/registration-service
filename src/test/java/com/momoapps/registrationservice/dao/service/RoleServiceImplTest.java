
package com.momoapps.registrationservice.dao.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

import com.momoapps.registrationservice.dao.RoleRepository;
import com.momoapps.registrationservice.model.Role;
import com.momoapps.registrationservice.service.RoleServiceImpl;

public class RoleServiceImplTest {

	RoleRepository roleRepository = Mockito.mock(RoleRepository.class);

	@Test
	public void test_getAllRoles() {

		RoleServiceImpl roleService = new RoleServiceImpl(roleRepository);

		Role role = new Role();
		role.setRoleName("USER_ROLE");
		List<Role> roles = Arrays.asList(role);

		Mockito.when(roleRepository.findAll()).thenReturn(roles);
		Collection<Role> result = roleService.getAllRoles();

		Assertions.assertThat(result).isNotNull().hasSize(1);
	}

	@Test
	public void test_getAllRolesStream() {

		RoleServiceImpl roleService = new RoleServiceImpl(roleRepository);

		Role role = new Role();
		role.setRoleName("USER_ROLE");
		List<Role> roles = Arrays.asList(role);

		Mockito.when(roleRepository.findAll()).thenReturn(roles);
		Collection<Role> result = roleService.getAllRoles();

		Assertions.assertThat(result).isNotNull().hasSize(1);
	}
}