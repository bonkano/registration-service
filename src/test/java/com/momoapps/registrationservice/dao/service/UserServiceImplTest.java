package com.momoapps.registrationservice.dao.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.momoapps.registrationservice.dao.RoleRepository;
import com.momoapps.registrationservice.dao.UserRepository;
import com.momoapps.registrationservice.exception.BusinessResourceException;
import com.momoapps.registrationservice.model.Role;
import com.momoapps.registrationservice.model.User;
import com.momoapps.registrationservice.service.UserService;
import com.momoapps.registrationservice.service.UserServiceImpl;

//@RunWith(SpringRunner.class) //pas besoin car on a fait l'autowired par constructeur sur UserServiceImpl
public class UserServiceImplTest {

	private UserService userService;
	private RoleRepository roleRepository;
	private UserRepository userRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Before
	public void setup() {
		userRepository = Mockito.mock(UserRepository.class);
		roleRepository = Mockito.mock(RoleRepository.class);
		bCryptPasswordEncoder = Mockito.mock(BCryptPasswordEncoder.class);
		userService = new UserServiceImpl(userRepository, roleRepository, bCryptPasswordEncoder);
	}

	@Test
	public void testFindAllUsers() throws Exception {
		User user = new User("Dupont", "password", 1);
		Role role = new Role();
		role.setRoleName("USER_ROLE");
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		user.setRoles(roles);
		List<User> allUsers = Arrays.asList(user);
		Mockito.when(userRepository.findAll()).thenReturn(allUsers);
		Collection<User> users = userService.getAllUsers();
		assertNotNull(users);
		assertEquals(users, allUsers);
		assertEquals(users.size(), allUsers.size());
		verify(userRepository).findAll();
	}

	@Test
	public void testSaveUser() throws Exception {
		User user = new User("Dupont", "password", 1);
		Role role = new Role();
		role.setRoleName("USER_ROLE");
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		User userMock = new User(1L, "Dupont", "password", 1);
		userMock.setRoles(roles);
		Mockito.when(roleRepository.getAllRolesStream()).thenReturn(roles.stream());
		Mockito.when(userRepository.save((user))).thenReturn(userMock);
		User userSaved = userService.saveOrUpdateUser(user);
		assertNotNull(userSaved);
		assertEquals(userMock.getId(), userSaved.getId());
		assertEquals(userMock.getLogin(), userSaved.getLogin());
		assertEquals(userMock.getLogin(), userSaved.getLogin());
		assertEquals(userMock.getRoles(), userSaved.getRoles());
		verify(userRepository).save(any(User.class));
	}

	@Test(expected = BusinessResourceException.class)
	public void testSaveUser_existing_login_throws_error() throws Exception {
		User user = new User("Dupont", "password", 1);
		Role role = new Role();
		role.setRoleName("USER_ROLE");
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		Mockito.when(roleRepository.getAllRolesStream()).thenReturn(roles.stream());
		Mockito.when(userRepository.save((user))).thenThrow(new DataIntegrityViolationException("Duplicate Login"));
		userService.saveOrUpdateUser(user);
		verify(userRepository).save(any(User.class));
	}

	@Test
	public void testFindUserByLogin() {
		User user = new User("Dupont", "password", 1);
		Mockito.when(userRepository.findByLogin(user.getLogin())).thenReturn(Optional.ofNullable(user));
		Optional<User> userFromDB = userService.findByLogin(user.getLogin());
		assertNotNull(userFromDB);
		assertThat(userFromDB.get().getLogin(), is(user.getLogin()));
		verify(userRepository).findByLogin(any(String.class));
	}

	@Test
	public void testUpdateUser() throws Exception {
		User userToUpdate = new User(1L, "NewDupont", "newPassword", 1);
		Role role = new Role();
		role.setRoleName("USER_ROLE");
		Set<Role> roles = new HashSet<>();
		roles.add(role);

		User userFoundById = new User(1L, "OldDupont", "oldpassword", 1);
		userFoundById.setRoles(roles);

		User userUpdated = new User(1L, "NewDupont", "newPassword", 1);
		userUpdated.setRoles(roles);

		Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(userFoundById));
		Mockito.when(bCryptPasswordEncoder.matches(any(String.class), any(String.class))).thenReturn(false);
		Mockito.when(bCryptPasswordEncoder.encode(any(String.class))).thenReturn("newPassword");
		Mockito.when(userRepository.save((userToUpdate))).thenReturn(userUpdated);
		User userFromDB = userService.saveOrUpdateUser(userToUpdate);
		assertNotNull(userFromDB);
		verify(userRepository).save(any(User.class));
		assertEquals(new Long(1), userFromDB.getId());
		assertEquals("NewDupont", userFromDB.getLogin());
		assertEquals("newPassword", userFromDB.getPassword());
		assertEquals(new Integer(1), userFromDB.getActive());
		assertEquals(roles, userFromDB.getRoles());
	}

	@Test
	public void testDelete() throws Exception {
		User userTodelete = new User(1L, "Dupont", "password", 1);
		Mockito.doNothing().when(userRepository).deleteById(userTodelete.getId());
		userService.deleteUser(userTodelete.getId());

		verify(userRepository).deleteById(any(Long.class));
	}
}