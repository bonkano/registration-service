package com.momoapps.registrationservice.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.momoapps.registrationservice.model.User;

@RunWith(SpringRunner.class) // permet d'établir une liaison entre JUnit et Spring
@DataJpaTest
public class UserRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private UserRepository userRepository;
	User user = new User("Dupont", "password", 1);

	@Before
	public void setup() {
		entityManager.persist(user);// on sauvegarde l'objet user au début de chaque test
		entityManager.flush();
	}

	@Test
	public void testFindAllUsers() {
		List<User> users = userRepository.findAll();
		assertThat(4, is(users.size()));// on a 3 Users dans le fichier d'initialisation data.sql et un utilisateur
										// ajouté lors du setup du test
	}

	@Test
	public void testSaveUser() {
		User user = new User("Paul", "password", 1);
		User userSaved = userRepository.save(user);
		assertNotNull(userSaved.getId());
		assertThat("Paul", is(userSaved.getLogin()));
	}

	@Test
	public void testFindByLogin() {
		Optional<User> userFromDB = userRepository.findByLogin("login2");
		assertThat("login2", is(userFromDB.get().getLogin()));// login2 a été crée lors de l'initialisation du fichier
																// data.sql
	}

	@Test
	public void testFindById() {
		Optional<User> userFromDB = userRepository.findById(user.getId());
		assertThat(user.getLogin(), is(userFromDB.get().getLogin()));// user a été crée lors du setup
	}

	@Test
	public void testFindBy_Unknow_Id() {
		Optional<User> userFromDB = userRepository.findById(50L);
		assertEquals(Optional.empty(), Optional.ofNullable(userFromDB).get());
	}

	@Test
	public void testDeleteUser() {
		userRepository.deleteById(user.getId());
		Optional<User> userFromDB = userRepository.findByLogin(user.getLogin());
		assertEquals(Optional.empty(), Optional.ofNullable(userFromDB).get());
	}

	@Test
	public void testUpdateUser() {// Test si le compte utilisateur est désactivé
		Optional<User> userToUpdate = userRepository.findByLogin(user.getLogin());
		userToUpdate.get().setActive(0);
		userRepository.save(userToUpdate.get());
		Optional<User> userUpdatedFromDB = userRepository.findByLogin(userToUpdate.get().getLogin());
		assertNotNull(userUpdatedFromDB);
		assertThat(0, is(userUpdatedFromDB.get().getActive()));
	}
}