package com.momoapps.registrationservice.integrationtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import com.momoapps.registrationservice.model.Role;
import com.momoapps.registrationservice.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserControllerIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;
	private static final String URL = "http://localhost:8080";// url du serveur REST. Cette url peut être celle d'un //
																// serveur distant

	private String getURLWithPort(String uri) {
		return URL + uri;
	}

	@Test
	public void testFindAllUsers() throws Exception {

		ResponseEntity<Object> responseEntity = restTemplate
				.getForEntity(getURLWithPort("/springboot-restserver/user/users"), Object.class);
		assertNotNull(responseEntity);
		@SuppressWarnings("unchecked")
		Collection<User> userCollections = (Collection<User>) responseEntity.getBody();
		assertEquals("Réponse inattendue", HttpStatus.FOUND.value(), responseEntity.getStatusCodeValue());
		assertNotNull(userCollections);
		assertEquals(4, userCollections.size());
		// on a bien 3 utilisateurs initialisés par les scripts data.sql + un nouvel
		// utilisateur crée dans testSaveUser
	}

	@Test
	public void testSaveUser() throws Exception {

		User user = new User("PIPO", "newPassword", 1);
		ResponseEntity<User> userEntitySaved = restTemplate
				.postForEntity(getURLWithPort("/springboot-restserver/user/users"), user, User.class);
		assertNotNull(userEntitySaved);
		// On vérifie le code de réponse HTTP est celui attendu
		assertEquals("Réponse inattendue", HttpStatus.CREATED.value(), userEntitySaved.getStatusCodeValue());
		User userSaved = userEntitySaved.getBody();
		assertNotNull(userSaved);
		assertEquals(user.getLogin(), userSaved.getLogin());
		assertEquals("ROLE_USER", userSaved.getRoles().iterator().next().getRoleName());// on vérifie qu'un role par
	}

	@Test
	public void testFindByLoginAndPassword() throws Exception {

		User userTofindByLoginAndPassword = new User("admin@admin.com", "admin");
		ResponseEntity<User> responseEntity = restTemplate.postForEntity(
				getURLWithPort("/springboot-restserver/user/users/login"), userTofindByLoginAndPassword, User.class);
		assertNotNull(responseEntity);
		User userFound = responseEntity.getBody();

		assertEquals("Réponse inattendue", HttpStatus.FOUND.value(), responseEntity.getStatusCodeValue());
		assertNotNull(userFound);
		assertEquals(new Long(1), userFound.getId());
	}

	@Test
	public void testFindByLoginAndPassword_notFound() throws Exception {

		User userTofindByLoginAndPassword = new User("unknowUser", "password3");
		ResponseEntity<User> responseEntity = restTemplate.postForEntity(
				getURLWithPort("/springboot-restserver/user/users/login"), userTofindByLoginAndPassword, User.class);
		assertNotNull(responseEntity);
		assertEquals("Réponse inattendue", HttpStatus.NOT_FOUND.value(), responseEntity.getStatusCodeValue());
	}

	@Test
	public void testUpdateUser() throws Exception {

		ResponseEntity<User> responseEntityToUpdate = restTemplate.postForEntity(
				getURLWithPort("/springboot-restserver/user/users/login"), new User("login3", "password3"), User.class);
		User userFromDBtoUpdate = responseEntityToUpdate.getBody();
		// on met à jour l'utilisateur en lui attribuant le role admin, nouveau login et mot de passe
		userFromDBtoUpdate.setLogin("newLogin");
		userFromDBtoUpdate.setPassword("newPassword");
		userFromDBtoUpdate.setActive(1);
		Role role = new Role("ROLE_ADMIN");
		userFromDBtoUpdate.getRoles().add(role);

		URI uri = UriComponentsBuilder.fromHttpUrl(URL).path("/springboot-restserver/user/users").build().toUri();

		HttpEntity<User> requestEntity = new HttpEntity<User>(userFromDBtoUpdate);

		ResponseEntity<User> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, requestEntity, User.class);
		assertNotNull(responseEntity);
		User userUpdated = responseEntity.getBody();
		assertNotNull(userUpdated);
		assertEquals("Réponse inattendue", HttpStatus.OK.value(), responseEntity.getStatusCodeValue());
		assertEquals(userFromDBtoUpdate.getLogin(), userUpdated.getLogin());
	}

	@Test
	public void testDeleteUser() throws Exception {

		URI uri = UriComponentsBuilder.fromHttpUrl(URL).path("/springboot-restserver/user/users")
				.queryParam("id", new Long(2)).build().toUri();

		ResponseEntity<Void> responseEntity = restTemplate.exchange(uri, HttpMethod.DELETE, null, Void.class);
		assertNotNull(responseEntity);
		assertEquals("Réponse inattendue", HttpStatus.GONE.value(), responseEntity.getStatusCodeValue());
	}
}