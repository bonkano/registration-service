package com.momoapps.registrationservice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
//for HTTP methods
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//for HTTP status
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.momoapps.registrationservice.model.Role;
import com.momoapps.registrationservice.model.User;
import com.momoapps.registrationservice.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserController.class, excludeAutoConfiguration = SecurityAutoConfiguration.class)
public class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;
	private ObjectMapper objectMapper;

	@MockBean
	private UserService userService;

	User user;

	@Before
	public void setUp() {
		// Initialisation du setup avant chaque test
		Role role = new Role();// initialisation du role utilisateur
		role.setRoleName("USER_ROLE");
		Set<Role> roles = new HashSet<>();
		roles.add(role);
		user = new User(1L, "Dupont", "password", 1);
		user.setRoles(roles);
		List<User> allUsers = Arrays.asList(user);
		objectMapper = new ObjectMapper();

		// Mock de la couche de service
		when(userService.getAllUsers()).thenReturn(allUsers);
		when(userService.findUserById(any(Long.class))).thenReturn(Optional.of(user));

	}

	@Test
	public void testFindAllUsers() throws Exception {

		MvcResult result = mockMvc.perform(get("/user/users").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isFound()).andReturn();

		// ceci est une redondance, car déjà vérifié par: isFound())
		assertEquals("Réponse incorrecte", HttpStatus.FOUND.value(), result.getResponse().getStatus());
		verify(userService).getAllUsers();
		assertNotNull(result);
		Collection<User> users = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<Collection<User>>() {
				});
		assertNotNull(users);
		assertEquals(1, users.size());
		User userResult = users.iterator().next();
		assertEquals(user.getLogin(), userResult.getLogin());
		assertEquals(user.getPassword(), userResult.getPassword());

	}

	@Test
	public void testSaveUser() throws Exception {

		User userToSave = new User("Dupont", "password", 1);
		String jsonContent = objectMapper.writeValueAsString(userToSave);
		when(userService.saveOrUpdateUser(any(User.class))).thenReturn(user);
		MvcResult result = mockMvc
				.perform(post("/user/users").contentType(MediaType.APPLICATION_JSON).content(jsonContent))
				.andExpect(status().isCreated()).andReturn();

		assertEquals("Erreur de sauvegarde", HttpStatus.CREATED.value(), result.getResponse().getStatus());
		verify(userService).saveOrUpdateUser(any(User.class));
		User userResult = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<User>() {
		});
		assertNotNull(userResult);
		assertEquals(userToSave.getLogin(), userResult.getLogin());
		assertEquals(userToSave.getPassword(), userResult.getPassword());

	}

	@Test
	public void testFindUserByLogin() throws Exception {
		when(userService.findByLoginAndPassword("Dupont", "password")).thenReturn(Optional.ofNullable(user));
		User userTofindByLogin = new User("Dupont", "password");
		String jsonContent = objectMapper.writeValueAsString(userTofindByLogin);
		// on execute la requête
		MvcResult result = mockMvc
				.perform(post("/user/users/login").contentType(MediaType.APPLICATION_JSON).content(jsonContent))
				.andExpect(status().isFound()).andReturn();

		assertEquals("Erreur de sauvegarde", HttpStatus.FOUND.value(), result.getResponse().getStatus());
		verify(userService).findByLoginAndPassword(any(String.class), any(String.class));
		User userResult = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<User>() {
		});
		assertNotNull(userResult);
		assertEquals(new Long(1), userResult.getId());
		assertEquals(userTofindByLogin.getLogin(), userResult.getLogin());
		assertEquals(userTofindByLogin.getPassword(), userResult.getPassword());
	}

	@Test
	public void testDeleteUser() throws Exception {

		MvcResult result = mockMvc.perform(delete("/user/users/").param("id", "1")).andExpect(status().isGone())
				.andReturn();
		assertEquals("Erreur de suppression", HttpStatus.GONE.value(), result.getResponse().getStatus());
		verify(userService).deleteUser(any(Long.class));
	}

	@Test
	public void testUpdateUser() throws Exception {

		User userToUpdate = new User("Toto", "password", 0);
		User userUpdated = new User(2L, "Toto", "password", 0);
		String jsonContent = objectMapper.writeValueAsString(userToUpdate);
		when(userService.saveOrUpdateUser(userToUpdate)).thenReturn(userUpdated);
		// on execute la requête
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.put("/user/users/").contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(jsonContent))
				.andExpect(status().isOk()).andReturn();

		verify(userService).saveOrUpdateUser(any(User.class));
		User userResult = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<User>() {
		});
		assertNotNull(userResult);
		assertEquals(new Long(2), userResult.getId());
		assertEquals(userToUpdate.getLogin(), userResult.getLogin());
		assertEquals(userToUpdate.getPassword(), userResult.getPassword());
		assertEquals(userToUpdate.getActive(), userResult.getActive());
	}

}